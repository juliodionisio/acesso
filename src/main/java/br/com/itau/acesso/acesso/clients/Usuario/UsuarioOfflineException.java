package br.com.itau.acesso.acesso.clients.Usuario;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.BAD_GATEWAY, reason = "O sistema de usuários se encontra offline")
public class UsuarioOfflineException extends RuntimeException {
}
