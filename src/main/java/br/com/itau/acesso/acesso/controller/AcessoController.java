package br.com.itau.acesso.acesso.controller;

import br.com.itau.acesso.acesso.model.Acesso;
import br.com.itau.acesso.acesso.service.AcessoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/acesso")
public class AcessoController {

    @Autowired
    AcessoService acessoService;


    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Acesso cadastrar(@Valid @RequestBody Acesso acesso){
        acesso = acessoService.cadastrar(acesso);
        return acesso;
    }

    @DeleteMapping
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deletar(@RequestParam(name = "cliente_id") Long usuarioID,
                        @RequestParam(name = "porta_id") Long portaID){
        Acesso acesso = new Acesso();
        acesso.setUsuarioID(usuarioID);
        acesso.setPortaID(portaID);
        acessoService.deletar(acesso);
    }

    @GetMapping
    public Acesso buscar(@RequestParam(name = "cliente_id") Long usuarioID,
                         @RequestParam(name = "porta_id") Long portaID){
        Acesso acesso = new Acesso();
        acesso.setUsuarioID(usuarioID);
        acesso.setPortaID(portaID);
        return acessoService.buscar(acesso);
    }


}
