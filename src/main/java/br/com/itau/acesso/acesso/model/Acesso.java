package br.com.itau.acesso.acesso.model;

import javax.persistence.*;

@Entity
public class Acesso {



    @Column
    private Long portaID;

    @Column
    private Long usuarioID;

    public Acesso() {
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    public Long getPortaID() {
        return portaID;
    }

    public void setPortaID(Long portaID) {
        this.portaID = portaID;
    }

    public Long getUsuarioID() {
        return usuarioID;
    }

    public void setUsuarioID(Long usuarioID) {
        this.usuarioID = usuarioID;
    }
}
