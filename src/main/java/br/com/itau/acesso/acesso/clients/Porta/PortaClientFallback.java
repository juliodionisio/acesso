package br.com.itau.acesso.acesso.clients.Porta;

import br.com.itau.acesso.acesso.model.Porta;

public class PortaClientFallback implements PortaClient {

    @Override
    public Porta getPortaByID(Long id) {

        throw new PortaOfflineException();
    }
}
