package br.com.itau.acesso.acesso.repository;

import br.com.itau.acesso.acesso.model.Acesso;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface AcessoRepository extends CrudRepository<Acesso, Long> {

    Optional<Acesso> findByPortaIDAndUsuarioID(Long portaID, Long usuarioID);
}
