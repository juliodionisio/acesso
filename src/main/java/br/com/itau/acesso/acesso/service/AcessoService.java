package br.com.itau.acesso.acesso.service;

import br.com.itau.acesso.acesso.clients.Porta.PortaClient;
import br.com.itau.acesso.acesso.clients.Usuario.UsuarioClient;
import br.com.itau.acesso.acesso.model.Acesso;
import br.com.itau.acesso.acesso.model.Porta;
import br.com.itau.acesso.acesso.model.Usuario;
import br.com.itau.acesso.acesso.repository.AcessoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class AcessoService {

    @Autowired
    private AcessoRepository acessoRepository;

    @Autowired
    private PortaClient portaClient;

    @Autowired
    private UsuarioClient usuarioClient;

    public Acesso cadastrar(Acesso acesso) {
        Porta porta = portaClient.getPortaByID(acesso.getPortaID());
        Usuario usuario = usuarioClient.getUsuarioByID(acesso.getUsuarioID());
        acesso.setPortaID(porta.getId());
        acesso.setUsuarioID(usuario.getId());

        return acessoRepository.save(acesso);
    }

    public void deletar(Acesso acesso){

        Optional<Acesso> acessoDelete = acessoRepository.findByPortaIDAndUsuarioID(acesso.getPortaID(), acesso.getUsuarioID());

        if(!acessoDelete.isPresent()){
            throw new RuntimeException();
        }

        acessoRepository.delete(acessoDelete.get());
    }

    public Acesso buscar(Acesso acesso){
        Optional<Acesso> acessoObjeto = acessoRepository.findByPortaIDAndUsuarioID(acesso.getPortaID(), acesso.getUsuarioID());

        if(!acessoObjeto.isPresent()){
            throw new RuntimeException();
        }

        return acessoObjeto.get();

    }



}
