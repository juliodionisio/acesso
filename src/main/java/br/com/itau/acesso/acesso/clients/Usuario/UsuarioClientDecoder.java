package br.com.itau.acesso.acesso.clients.Usuario;

import br.com.itau.acesso.acesso.clients.Porta.InvalidPortaException;
import feign.Response;
import feign.codec.ErrorDecoder;

public class UsuarioClientDecoder implements ErrorDecoder {

    private ErrorDecoder errorDecoder = new ErrorDecoder.Default();

    @Override
    public Exception decode(String s, Response response) {
        if (response.status() == 404) {
            throw new InvalidPortaException();
        }else{
            return errorDecoder.decode(s, response);
        }
    }
}
