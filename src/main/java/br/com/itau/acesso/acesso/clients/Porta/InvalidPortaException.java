package br.com.itau.acesso.acesso.clients.Porta;


import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.BAD_REQUEST, reason = "A porta informada é inválida")
public class InvalidPortaException extends RuntimeException{
}
