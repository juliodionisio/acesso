package br.com.itau.acesso.acesso.clients.Usuario;

import br.com.itau.acesso.acesso.model.Usuario;

public class UsuarioClientFallback implements UsuarioClient{

    @Override
    public Usuario getUsuarioByID(Long id) {

        throw new UsuarioOfflineException();
    }
}
