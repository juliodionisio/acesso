package br.com.itau.acesso.acesso.clients.Usuario;


import br.com.itau.acesso.acesso.clients.Porta.PortaClientConfiguration;
import br.com.itau.acesso.acesso.model.Porta;
import br.com.itau.acesso.acesso.model.Usuario;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "usuario", configuration = UsuarioClientConfiguration.class)
public interface UsuarioClient {

    @GetMapping("/usuario/{id}")
    Usuario getUsuarioByID(@PathVariable Long id);
}
