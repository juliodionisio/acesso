package br.com.itau.acesso.acesso.clients.Usuario;

import br.com.itau.acesso.acesso.clients.Porta.PortaClientDecoder;
import br.com.itau.acesso.acesso.clients.Porta.PortaClientFallback;
import feign.Feign;
import feign.RetryableException;
import feign.codec.ErrorDecoder;
import io.github.resilience4j.feign.FeignDecorators;
import io.github.resilience4j.feign.Resilience4jFeign;
import org.springframework.context.annotation.Bean;

public class UsuarioClientConfiguration {

    @Bean
    public ErrorDecoder getErrorDecoder() {
        return new UsuarioClientDecoder();
    }

    @Bean
    public Feign.Builder builder() {
        FeignDecorators feignDecorators = FeignDecorators.builder()
                .withFallback(new UsuarioClientFallback(), RetryableException.class)
                .build();

        return Resilience4jFeign.builder(feignDecorators);
    }
}
