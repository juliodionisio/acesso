package br.com.itau.acesso.acesso.clients.Usuario;


import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.BAD_REQUEST, reason = "O usuário informado é inválido")
public class InvalidUsuarioException extends RuntimeException{
}
